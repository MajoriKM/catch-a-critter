﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;


namespace catch_a_critter
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        const int MAX_CRITTERS = 20;
        Critter[] critterPool = new Critter[MAX_CRITTERS]; // an array of critter references
        Score ourScore;
        Button startButton = null;
        Timer gameTimer = null;
        bool playing = false;
        const float SPAWN_DELAY = 3f;
        const float GAME_LENGTH = 10f;
        float timeUntilSpawn = SPAWN_DELAY;
        int currentCritterIndex = 0;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            ourScore = new Score();
            ourScore.LoadContent(Content);
            //--------------------------------------------------

            // create timer object and load its content
            gameTimer = new Timer();
            gameTimer.LoadContent(Content);
            gameTimer.SetTimer(GAME_LENGTH);
           
            gameTimer.ourTimerCallBack += EndGame;

            // Initialise random number generator for species
            Random rand = new Random();

            //create critter objects
            for( int i = 0; i < MAX_CRITTERS; ++i)
            {
                Critter.Species newSpecies = (Critter.Species)rand.Next(0, (int)Critter.Species.NUM);


                Critter newcritter = new Critter(ourScore, newSpecies);

                newcritter.LoadContent(Content);

                //add new created criteer to pool
                critterPool[i] = newcritter;

                
            }
            // create and load button
            startButton = new Button();
            startButton.LoadContent(Content);
            // set the function that is called when button is clicked 
            //(+= means added to a list of function)
            startButton.ourButtonCallBack += StartGame;


            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            //ourCritter.Input();


            if (playing == true)
            {
                gameTimer.Update(gameTime);

                timeUntilSpawn -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (timeUntilSpawn <= 0)
                {
                    timeUntilSpawn = SPAWN_DELAY;


                    //spawn next critter in list
                    critterPool[currentCritterIndex].Spawn(Window);
                    ++currentCritterIndex;

                    if (currentCritterIndex >= critterPool.Length)
                    {
                        currentCritterIndex = 0;
                    }
                }

                foreach (Critter eachCritter in critterPool)
                {
                    eachCritter.Input();
                }
            }
            else
            {
                startButton.Input();
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            foreach (Critter eachCritter in critterPool)
            {
                eachCritter.Draw(spriteBatch);
            }

            startButton.Draw(spriteBatch);
            //-----------------------------------------------------------------
            ourScore.Draw(spriteBatch);

            gameTimer.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        void StartGame()
        {
            playing = true;
            gameTimer.StartTimer();
            ourScore.ResetScore();
        }
        void EndGame()
        {
            playing = false;

            //show button
            startButton.Show();

            //remove all active critters
            foreach(Critter eachCritter in critterPool)
            {
                eachCritter.Despawn();
            }

            gameTimer.SetTimer(GAME_LENGTH);
        }
    }
}
