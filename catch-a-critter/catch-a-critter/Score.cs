﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace catch_a_critter
{
    class Score
    {

        //---------------------------------
        // Data
        //---------------------------------
        int value = 0;
        Vector2 position = new Vector2(10, 10);
        SpriteFont font;



        // Behaviour
        public void LoadContent(ContentManager content)
        {
            font = content.Load<SpriteFont>("fonts/maiFont");
        }

        public void addScore(int toAdd)
        {

            // adds provided number to the current score
            value += toAdd;

        }

        public void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.DrawString(font, "Score: " + value.ToString(), position, Color.White);

        }

        public void ResetScore()
        {
            value = 0;
        }

    }
}
