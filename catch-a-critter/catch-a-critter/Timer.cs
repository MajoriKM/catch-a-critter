﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace catch_a_critter
{
    class Timer
    {

        //---------------------------------
        // Data
        //---------------------------------
        float timeRemaining = 0f;
        Vector2 position = new Vector2(200, 10);
        SpriteFont font;
        bool running = false;

        public delegate void TimeUp();
        public TimeUp ourTimerCallBack;

        // Behaviour
        public void LoadContent(ContentManager content)
        {
            font = content.Load<SpriteFont>("fonts/maiFont");
        }
        //-----------------------------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            int timeInt = (int)timeRemaining;

            spriteBatch.DrawString(font, 
                "Time: " + timeInt.ToString(), 
                position, 
                Color.White);

        }
        //----------------------------------------------------------

        public void Update(GameTime gameTime)
        {
            if (running == true)
            {
                timeRemaining -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                //if time has run out...
                if (timeRemaining <= 0)
                {
                    running = false;
                    timeRemaining = 0;

                    // the something to do:
                    if (ourTimerCallBack != null)
                    {
                        ourTimerCallBack();
                    }
                }
            }
        }

        //-----------------------------------------
        public void StartTimer()
        {
            running = true;
        }
        //----------------------------------------------
        public void SetTimer (float newTime)
        {
            timeRemaining = newTime;
        }

        

    }
}
