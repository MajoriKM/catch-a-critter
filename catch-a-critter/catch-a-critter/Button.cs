﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;


namespace catch_a_critter
{
    class Button
    {
        // DATA
        Texture2D buttonImg;
        Vector2 position = Vector2.Zero;
        SoundEffect click = null;
        bool visible = true;

        // Delegates
        // the delegate definition states what type of functions are allowed for this delegate
        public delegate void OnClick();
        public OnClick ourButtonCallBack;

        // BEHAVIOUR
        public void LoadContent(ContentManager content)
        {

            buttonImg = content.Load<Texture2D>("graphics/button");

            click = content.Load<SoundEffect>("audio/buttonclick");
        }

        //------------------------------------------------------------------

        public void Draw(SpriteBatch spriteBatch)
        {
            if (visible == true)
            {
                spriteBatch.Draw(buttonImg, position, Color.White);
            }
        }


        public void Input()
        {
            MouseState currentState = Mouse.GetState();

            Rectangle bounds = new Rectangle((int)position.X,
                (int)position.Y,
                buttonImg.Width,
                buttonImg.Height);

            if (currentState.LeftButton == ButtonState.Pressed
                && bounds.Contains(currentState.X, currentState.Y)
                && visible == true)
            {
                //critter click
                click.Play();

                // hide button
                visible = false;

                // start the game
                if ( ourButtonCallBack != null)
                ourButtonCallBack();
            }

        }

        public void Show()
        {
            visible = true;
        }
    }
}
