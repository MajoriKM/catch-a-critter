﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;


namespace catch_a_critter
{
    class Critter
    {
        // Type Definitions 
        public enum Species  // special type of integer with specific values allowed that have names
        {
            CHICKEN, // =0
            HORSE, // = 1
            ELEPHANT, // = 2

            //--------------
            NUM       // = 3
        }



        //Data

        Texture2D image = null;
        Vector2 position = Vector2.Zero;
        bool alive = false;
        Score scoreObject = null;
        int critterValue = 10;
        SoundEffect clckSFX;
        Species ourSpecies = Species.CHICKEN;

        //behaviour

        public Critter(Score newScore, Species newSpecies)
        {
            // constructer. called when the object is created
            // no return type
            // helps decide how the object is set up
            //can have arguments
            scoreObject = newScore;
            ourSpecies = newSpecies;

        }

        public void LoadContent(ContentManager content)
        {
            image = content.Load<Texture2D>("graphics/chicken");

            switch(ourSpecies)
            {
                case Species.CHICKEN:
                    image = content.Load<Texture2D>("graphics/chicken");
                    critterValue = 10;
                    break;
                case Species.HORSE:
                    image = content.Load<Texture2D>("graphics/horse");
                    critterValue = 5;
                    break;
                case Species.ELEPHANT:
                    image = content.Load<Texture2D>("graphics/elephant");
                    critterValue = 20;
                    break;
                default:
                    // should never happen
                    break;
            }




            clckSFX = content.Load<SoundEffect>("audio/buttonclick");
        }
        //--------------------------------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (alive)
                spriteBatch.Draw(image, position, Color.White);
        }
        //--------------------------------------------------------------
        public void Spawn(GameWindow window)
        {

            //set critter to be alive
            alive = true;

            int positionYMin = 0;
            int positionXMin = 0;
            int positionYMax = window.ClientBounds.Height - image.Height;
            int positionXMax = window.ClientBounds.Width - image.Width;

            Random rand = new Random();
            position.X = rand.Next(positionXMin, positionXMax);
            position.Y = rand.Next(positionYMin, positionYMax);
        }
        //--------------------------------------------------------------
        public void Despawn()
        {
            // set critter to not alive so to not draw or be clickable
            alive = false;
        }
        //-------------------------------------------------------------
        public void Input()
        {
            MouseState currentState = Mouse.GetState();

            Rectangle critterBounds = new Rectangle((int)position.X,
                (int)position.Y,
                image.Width,
                image.Height);

            if (currentState.LeftButton == ButtonState.Pressed 
                && critterBounds.Contains(currentState.X, currentState.Y) 
                && alive == true)
            {
                //critter click
                clckSFX.Play();

                //despawn critter
                Despawn();

                // add to score
                scoreObject.addScore(critterValue);
                
            }
        }

    }
}
